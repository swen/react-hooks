import { useState, useCallback, ChangeEvent, FormEvent } from "react";

// Handles values and changes for form inputs as well as submitting
export const useForm = <T extends Object>(
  onSubmit: (values: T) => void,
  initialValues: T
) => {
  const [values, setValues] = useState<T>(initialValues);

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      setValues({
        ...values,
        [e.currentTarget.name]: e.currentTarget.value
      });
    },
    [setValues, values]
  );

  // TODO: handle onBlur for validation purposes.
  // TODO: track error for each value in values for validation purposes
  // handle validation (debounce, sync/async)

  const handleSubmit = useCallback(
    (e: FormEvent) => {
      e.preventDefault(); // necessary to prevent browser default behavior to POST the form values and refresh the page.
      onSubmit(values);
    },
    [onSubmit, values]
  );

  // helper to manage props for form inputs
  const getPropsFor = useCallback(
    (name: keyof T) => {
      return {
        onChange: handleChange,
        value: values[name],
        name
      };
    },
    [handleChange, values]
  );

  return {
    getPropsFor,
    handleSubmit
  };
};
