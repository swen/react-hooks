import React, { FC, useCallback } from "react";
import { useForm } from "./use-form";

interface FormValues {
  name: string;
  password: string;
}

const inititalValues = {
  name: "",
  password: ""
};

export const UseFormExample: FC = () => {
  const submit = useCallback((values: FormValues) => console.log(values), []);
  const { getPropsFor, handleSubmit } = useForm(submit, inititalValues);

  return (
    <form onSubmit={handleSubmit}>
      <h2>UseForm example</h2>
      <input {...getPropsFor("name")} />
      <input {...getPropsFor("password")} />
      <button>submit</button>
    </form>
  );
};
