import { renderHook, act } from "@testing-library/react-hooks";
import { useForm } from "./use-form";
import { ChangeEvent, FormEvent } from "react";

interface FormValues {
  name: string;
  password: string;
}

describe("useForm", () => {
  let initialValues: FormValues;
  const submit = jest.fn();
  const preventDefault = jest.fn();

  beforeEach(() => {
    initialValues = {
      name: "test",
      password: "secret"
    };
    submit.mockReset();
    preventDefault.mockReset();
  });

  it("should handle initial values", async () => {
    const { result } = renderHook(() => useForm(submit, initialValues));

    expect(result.current.getPropsFor("name").value).toEqual(
      initialValues.name
    );
    expect(result.current.getPropsFor("password").value).toEqual(
      initialValues.password
    );
  });

  it("should handle change", async () => {
    const { result } = renderHook(() => useForm(submit, initialValues));

    const changeEvent = {
      currentTarget: {
        name: "name",
        value: "new_values"
      }
    } as ChangeEvent<HTMLInputElement>;

    act(() => {
      result.current.getPropsFor("name").onChange(changeEvent);
    });

    expect(result.current.getPropsFor("name").value).toEqual(
      changeEvent.currentTarget.value
    );
  });

  it("should handle submit", () => {
    const { result } = renderHook(() => useForm(submit, initialValues));

    const formEvent = ({
      preventDefault
    } as unknown) as FormEvent<HTMLFormElement>;

    act(() => result.current.handleSubmit(formEvent));

    expect(preventDefault).toBeCalledTimes(1);
    expect(submit).toHaveBeenCalledWith(initialValues);
  });
});
