import { useEffect } from "react";
import { useLazyQuery } from "./use-lazy-query";

/**
 * Handles a request to a REST resource on mount
 * of the component it is used in and when the url or options change.
 * For implementation details see useLazyQuery.

 * @param url of the resource to query
 * @param options of the request
 */
export const useQuery = <T>(url: string, options?: RequestInit) => {
  const [query, ...rest] = useLazyQuery<T>();

  useEffect(() => {
    query(url, options);
  }, [query, url, options]);

  return rest;
};
