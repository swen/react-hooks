import React, { FC } from "react";
import { useLazyQuery } from "./use-lazy-query";
import { Message, url } from "./api";

export const UseLazyQueryExample: FC = () => {
  const [getMessage, body, loading] = useLazyQuery<Message>();

  return (
    <div>
      <h2>useLazyQuery example</h2>
      {loading ? "loading..." : "not loading..."}
      <pre>{JSON.stringify(body, null, 2)}</pre>
      <button onClick={() => getMessage(url)}>click</button>
    </div>
  );
};
