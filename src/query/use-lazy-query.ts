import { useState, useCallback } from "react";

type UseLazyQuery<T> = [
  (url: string, options?: RequestInit) => void,
  T | null,
  boolean,
  { status: number; error: Error | null }
];

/**
 * Handles requests for REST resources. This includes making the request,
 * handling the loading and error states as well as storing the result.
 * To trigger a request call the query function.
 */
export const useLazyQuery = <T>(): UseLazyQuery<T> => {
  // will contain the json response of the request
  const [data, setData] = useState<T | null>(null);

  // will reflect if the request is still in progress or finished
  const [loading, setLoading] = useState(false);

  // will contain the HTTP status of the response or -1 if no response is present
  const [status, setStatus] = useState(-1);

  // will contain the error which may occur while making a
  // request or parsing the body of the response to json
  const [error, setError] = useState<Error | null>(null);

  const query = useCallback((url: string, options?: RequestInit) => {
    const execute = async (url: string, options?: RequestInit) => {
      // reset previous errors and status
      setError(null);
      setStatus(-1);
      // TODO: reset data as well?

      setLoading(true);

      try {
        // fetch only throws an error if there is a network issue, it will not throw on HTTP errors
        const response = await fetch(url, options);
        setStatus(response.status);

        // TODO: handle status codes where the backend does not send a json response
        // otherwise response.json() will throw an error
        // possibly set an error
        if (response.status !== 404) {
          const body = await response.json();
          setData(body);
        }
      } catch (e) {
        setError(e);
      } finally {
        setLoading(false);
      }
    };

    execute(url, options);
  }, []);

  return [query, data, loading, { status, error }];
};
