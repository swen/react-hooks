import React, { FC } from "react";
import { useQuery } from "./use-query";
import { Message, url } from "./api";

export const UseQueryExample: FC = () => {
  const [body, loading] = useQuery<Message>(url);

  return (
    <div>
      <h2>useQuery example</h2>
      {loading ? "loading..." : "not loading..."}
      <pre>{JSON.stringify(body, null, 2)}</pre>
    </div>
  );
};
