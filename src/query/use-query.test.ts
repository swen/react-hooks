import { renderHook } from "@testing-library/react-hooks";
import { useQuery } from "./use-query";

describe("useQuery", () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  it("should handle request", async () => {
    const url = "/mock/api";
    fetchMock.mockResponseOnce(JSON.stringify({ message: "test" }), {
      status: 201
    });
    const { result, waitForNextUpdate } = renderHook(() => useQuery(url));

    expect(result.current).toMatchSnapshot();
    expect(fetchMock).toHaveBeenCalledWith(url, undefined);

    await waitForNextUpdate();

    expect(result.current).toMatchSnapshot();
  });
});
