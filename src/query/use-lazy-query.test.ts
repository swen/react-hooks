import { renderHook, act } from "@testing-library/react-hooks";
import { useLazyQuery } from "./use-lazy-query";

describe("useLazyQuery", () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  it("should render initially", () => {
    const { result } = renderHook(() => useLazyQuery());

    expect(result.current).toMatchSnapshot();
    expect(fetchMock).not.toHaveBeenCalled();
  });

  it("should handle successful request", async () => {
    const url = "/mock/api";
    fetchMock.mockResponseOnce(JSON.stringify({ message: "test" }), {
      status: 201
    });
    const { result, waitForNextUpdate } = renderHook(() => useLazyQuery());

    const [query] = result.current;

    act(() => query(url));

    expect(result.current).toMatchSnapshot();
    expect(fetchMock).toHaveBeenCalledWith(url, undefined);

    await waitForNextUpdate();

    expect(result.current).toMatchSnapshot();
  });

  it("should handle network/fetch errors", async () => {
    const url = "/mock/api/error";
    fetchMock.mockRejectOnce(new Error("test_error"));
    const { result, waitForNextUpdate } = renderHook(() => useLazyQuery());

    const [query] = result.current;

    act(() => query(url));

    expect(result.current).toMatchSnapshot();
    expect(fetchMock).toHaveBeenCalledWith(url, undefined);

    await waitForNextUpdate();

    expect(result.current).toMatchSnapshot();
  });

  it("should not parse the body if the HTTP status equals 404", async () => {
    const url = "/mock/api/404";
    fetchMock.mockResponseOnce(JSON.stringify({ message: "test" }), {
      status: 404
    });
    const { result, waitForNextUpdate } = renderHook(() => useLazyQuery());

    const [query] = result.current;

    act(() => query(url));

    expect(result.current).toMatchSnapshot();
    expect(fetchMock).toHaveBeenCalledWith(url, undefined);

    await waitForNextUpdate();

    expect(result.current).toMatchSnapshot();
  });
});
