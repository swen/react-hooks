import React, { FC } from "react";
import { UseQueryExample } from "./query/use-query.example";
import { UseLazyQueryExample } from "./query/use-lazy-query.example";
import { UseFormExample } from "./forms/use-form.example";

export const App: FC = () => {
  return (
    <>
      <UseQueryExample />
      <UseLazyQueryExample />
      <UseFormExample />
    </>
  );
};
